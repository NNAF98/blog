<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['site_name', 'contact_number', 'contact_email', 'address'];// Generalement utilise les seeders
    //pour avoir des valeurs par defaut dans un site web 
}
