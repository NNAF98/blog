<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'nnaf98',
            'email' => 'nnaf98@nnaf98.com',
            'password' => bcrypt('password'),
            'admin' => 1    
        ]);

        App\Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/avatar.png',
            'about' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptates rem totam ipsam iste dolore harum debitis excepturi aut beatae. Dolor architecto reiciendis provident similique voluptas doloremque sapiente dolorum alias illum?',
            'facebook' => 'facebook.com',
            'youtube' => 'youtube.com'
        ]);
    }
}
