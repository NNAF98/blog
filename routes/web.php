<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/result', function(){
    $posts = \App\Post::where('title', 'like', '%'.request('query').'%')->get();
     
    return view('results')->with('posts', $posts)
                          ->with('query', request('query'))
                          ->with('title', \App\Setting::first()->site_name)
                          ->with('categories', \App\Category::take(5)->get())
                          ->with('setting', \App\Setting::first());
});

 //Category single page
 Route::get('/blog/categories/{id}', [
    'uses' => 'FrontEndController@categoriesPage',
    'as' => 'categoriespage'
]);

//Tags single page
Route::get('/blog/tag/{id}', [
    'uses' => 'FrontEndController@singleTag',
    'as' => 'tag.single'
]);

Route::get('/', [
    "uses" => "FrontEndController@index",
    "as" => "index"
]);

Auth::routes();

Route::get('/{slug}', [
    "uses" => "FrontendController@singlePost",
    "as" => "post.single"
]);

//Search post
Route::get('/test', function(){
    $posts = \App\Post::where('title', 'like', '%'.request('query').'%')->get();

    return view('results')->with('posts', $posts)
                          ->with('categories', \App\Category::take(5)->get())
                          ->with('setting', \App\Setting::first())
                          ->with('title', \App\Setting::first()->site_name)
                          ->with('query', request('query'));
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

    Route::get('/home', [
        'uses' => 'HomeController@index',
        'as' => 'home'
    ]);
    
    // posts route
    Route::get('/posts', [
        'uses' => 'PostController@index',
        'as'=> 'posts'
    ]);

    Route::get('/posts/trashed', [
        'uses' => 'PostController@trashed',
        'as'=> 'posts.trashed'
    ]);

    Route::get('/post/restore/{id}', [
        'uses' => 'PostController@restore',
        'as'=> 'post.restore'
    ]);

    Route::get('/post/kill/{id}', [
        'uses' => 'PostController@kill',
        'as'=> 'post.kill'
    ]);
    
    Route::get('/post/edit/{id}',[
        'uses' => 'PostController@edit',
        'as' => 'post.edit'
    ]);

    Route::post('/post/update/{id}',[
        'uses' => 'PostController@update',
        'as' => 'post.update'
    ]);

    Route::get('/post/delete/{id}',[
        'uses' => 'PostController@destroy',
        'as' => 'post.delete'
    ]);


    Route::get('/post/create', [
        'uses' => 'PostController@create',
        'as' => 'post.create'
    ]);
    
    Route::post('/post/store/', [
        'uses' => 'PostController@store',
        'as' => 'post.store'
    ]);

    // Categories route
    Route::get('/categories', [
        'uses' => 'CategoryController@index',
        'as' => 'categories'
    ]);

    Route::get('/category/create', [
        'uses' => 'CategoryController@create',
        'as' => 'category.create'
    ]);

    Route::post('/category/store', [
        'uses' => 'CategoryController@store',
        'as' => 'category.store'
    ]);

    Route::get('/category/edit/{id}', [
        'uses' => 'CategoryController@edit',
        'as' => 'category.edit'
    ]);

    Route::get('/category/delete/{id}', [
        'uses' => 'CategoryController@destroy',
        'as' => 'category.delete'
    ]);

    Route::post('/category/update/{id}', [
        'uses' => 'CategoryController@update',
        'as' => 'category.update'
    ]);

    // Tags route
    Route::get('/tags', [
        'uses' => 'TagController@index',
        'as' => 'tags'
    ]);

    Route::get('/tags/create', [
        'uses' => 'TagController@create',
        'as' => 'tag.create'
    ]);

    Route::post('/tags/store', [
        'uses' => 'TagController@store',
        'as' => 'tag.store'
    ]);

    Route::get('/tags/edit/{id}', [
        'uses' => 'TagController@edit',
        'as' => 'tag.edit'
    ]);

    Route::post('/tags/update/{id}', [
        'uses' => 'TagController@update',
        'as' => 'tag.update'
    ]);

    Route::get('/tags/delete/{id}', [
        'uses' => 'TagController@destroy',
        'as' => 'tag.delete'
    ]);

    //User route
    Route::get('/users', [
        'uses' => 'UserController@index',
        'as' => 'users'
    ]);

    Route::get('/user/create', [
        'uses' => 'UserController@create',
        'as' => 'user.create'
    ]);

    Route::post('/user/store', [
        'uses' => 'UserController@store',
        'as' => 'user.store'
    ]);

    Route::get('/user/admin/{id}', [
        'uses' => 'UserController@admin',
        'as' => 'user.admin'
    ]);  //Il faut a tout prix que je gere les flah notifications
    Route::get('/user/not-admin/{id}', [
        'uses' => 'UserController@not_admin',
        'as' => 'user.not.admin'
    ]);
    //Delete user

    Route::get('/user/delete/{id}', [
        'uses' => 'userController@destroy',
        'as' => 'user.delete'
    ]);
    // Don't forget d'implementer summernot pour remplir le contenu du post
    // Nobody use textarea for get post content, we must use WYSIWYG editor

    //User profile
    Route::get('/user/profile', [
        'uses' => 'ProfileController@index',
        'as' => 'user.profile'
    ]);

    Route::post('/user/profile/update', [
        'uses' => 'ProfileController@update',
        'as' => 'user.profile.update'
    ]);

    //Site settings
    Route::post('/settings/update' ,[
        'uses' => 'SettingController@update',
        'as' => 'settings.update'
    ]);

    Route::get('/settings' ,[
        'uses' => 'SettingController@index',
        'as' => 'settings'
    ]);
});

