<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Setting::create([
            'site_name' => 'Christ',
            'contact_number' => '698 28 00 99',
            'contact_email' => 'info@laravelBlog',
            'address' => 'Bikalla, SUD CAMEROUN'
        ]);
    }
}
