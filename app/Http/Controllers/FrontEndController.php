<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Post;
use App\Category;
use App\Setting;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function index()
    {
        return view('index')
                            ->with('title', Setting::first()->site_name)
                            ->with('categories', Category::take(5)->get())
                            ->with('first_post', Post::orderBy('created_at', 'desc')->first())
                            ->with('second_post', Post::orderBy('created_at', 'desc')->skip(1)->take(1)->get()->first())
                            ->with('third_post', Post::orderBy('created_at', 'desc')->skip(2)->take(1)->get()->first())
                            ->with('jesus_love', Category::find(4))
                            ->with('ThreeJs', Category::find(8))
                            ->with('setting', Setting::first());
    }

    public function singlePost($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $next_id = Post::where('id', '>', $post->id)->min('id'); //Recuperer tous les posts dont l'id est superieur a post->id, puis choisir celui qui le plus petit id
        $prev_id = Post::where('id', '<', $post->id)->max('id');
 
        return view('single')->with('post', $post)
                             ->with('title', $post->title)
                             ->with('categories', Category::take(5)->get())
                             ->with('setting', Setting::first())
                             ->with('title', Setting::first()->site_name)
                             ->with('next', Post::find($next_id))
                             ->with('prev', Post::find($prev_id))
                             ->with('tags', Tag::all());
    }

    public function categoriesPage($id)
    {
        $category = Category::find($id);

        return view('category')->with('category', $category)
                               ->with('tags', Tag::all())
                               ->with('setting', Setting::first())
                               ->with('categories', Category::take(5)->get())
                               ->with('title', Setting::first()->site_name);
    }

    public function singleTag($id)
    {
        $tag = Tag::find($id);

        return view('tag')->with('tag', $tag)
                          ->with('tags', Tag::all())
                          ->with('setting', Setting::first())
                          ->with('categories', Category::take(5)->get())
                          ->with('title', Setting::first()->site_name);
    }
}
