@extends('layouts.frontend')

@section('content')
    
    <div class="stunning-header stunning-header-bg-lightviolet">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Tag : {{ $tag->tag }}</h1>
        </div>
    </div>

    <div class="content-wrapper"></div>
    <div class="padded-50"></div>

    <div class="row">
        {{-- Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias ipsam vel ea, error consequatur totam dolorem asperiores explicabo ratione maiores. Temporibus beatae tempora quas libero a optio laudantium consectetur maxime! --}}
        <div class="case-item-wrap">
            @foreach ($tag->posts as $post)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="case-item">
                        <div class="case-item__thumb">
                            <img src="{{ $post->featured }}" alt="{{ $post->name }}">
                        </div>
                        <h6 class="case-item__title text-center"><a href="{{ route('post.single', ['slug' => $post->slug]) }}">{{ $post->title }}</a></h6>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
    
    <div class="content-wrapper"></div>
    <div class="padded-50"></div>

@endsection


    


