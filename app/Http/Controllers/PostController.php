<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return view('admin.posts.index')->with('posts', $posts);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $caterogies = Category::all();
        $tags = Tag::all();

        if ($caterogies->count() == 0 || $tags->count() == 0) {

            Session::flash('info', 'You must have some categories before attempting to create post.');

            return redirect()->back();
        }

        return view('admin.posts.create')->with('categories', $caterogies)
                                         ->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'title' => 'required',
            'featured' => 'required|image',
            'content' => 'required',
            'category_id' => 'required',
            'tags' => 'required'
        ]);
        /** 
         * Etapes pour uploader une image
         * 1- Recuperer le champ qui correspond a l'image dans une $chmp-img
         * 2- Recuperer le nom original de l'image: $new-chmp-img = time().$chmp-img->getClientOriginalName();
         * 3- Uploader le fichier dans notre application : $chmp-img->move('receivedDir', $new-chmp-img)
        */
        // Apres cela, tu sauvegarde juste le chemin de l'image dans l'app dans la BD: 
        $featured = $request->featured;

        $featured_new_name = time().$featured->getClientOriginalName();
        //Move file un our laravel app
        $featured->move('uploads/posts/', $featured_new_name);

        $post = Post::create([
            'title' => $request->title ,
            'content' => $request->content ,
            'featured' => 'uploads/posts/' .$featured_new_name ,
            'category_id' => $request->category_id,
            'slug' => str_slug($request->title),
            'user_id' => Auth::id()
        ]);
        
        $post->tags()->attach($request->tags);

        return redirect()->route('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $caterogies = Category::all();

        return view('admin.posts.edit', [
            'post' => $post,
            'categories' => $caterogies,
            'tags' => Tag::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required'
        ]);
        
        if ($request->hasFile('featured')) {
            $featured = $request->featured;
            $featured_new_name = time().$featured->getClientOriginalName();
            $featured->move('uploads/posts/',$featured_new_name);

            $post->featured = 'uploads/posts/' .$featured_new_name;
        }

        $post->title = $request->title;
        $post->content = $request->content;
        $post->category_id = $request->category_id;
        $post->slug = str_slug($post->title);

        $post->save();

        $post->tags()->sync($request->tags);

        return redirect()->route('posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Delete with softDelete: Trash
    public function destroy($id)
    {
        $post = Post::find($id);

        $post->delete();

        return redirect()->back();
    }

    //Display the trashed post
    public function trashed()
    {
        $posts = Post::onlyTrashed()->get();

        return view('admin.posts.trashed')->with('posts', $posts);
    }

    //Restore the post who are already trashed
    public function restore($id)
    {
        $post = Post::onlyTrashed()->where('id', $id)->first();

        $post->restore();

        return redirect()->route('posts');
    }

    //Delete permanently the post
    public function kill($id)
    {
        $post = Post::withTrashed()->where('id',$id)->first();

        $post->forceDelete();

        return redirect()->back();
    }
}
