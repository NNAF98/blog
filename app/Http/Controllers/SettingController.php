<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index()
    {
        $setting = Setting::first();
        return view('admin.settings.setting')->with('setting', $setting);
    }
    public function update()
    {
        $this->validate(request(), [
            'site_name' => 'required',
            'contact_number' => 'required',
            'contact_email' => 'required|email',
            'address' => 'required'
        ]);

        $setting = Setting::first();
        
        $setting->site_name = request()->site_name;
        $setting->contact_number = request()->contact_number;
        $setting->contact_email = request()->contact_email;
        $setting->address = request()->address;

        $setting->save();

        return redirect()->back();
    }
}
