@extends('layouts.frontend')

@section('content')
    {{-- A metter ailleur --}}
    <div class="container">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    {{-- First post setting --}}
                    <article class="hentry post post-standard has-post-thumbnail sticky">
    
                            <div class="post-thumb">
                                <img src="{{ $first_post->featured }}" alt="{{ $first_post->title }}">
                                <div class="overlay"></div>
                                <a href="{{ $first_post->featured }}" class="link-image js-zoom-image">
                                    <i class="seoicon-zoom"></i>
                                </a>
                                <a href="#" class="link-post">
                                    <i class="seoicon-link-bold"></i>
                                </a>        
                            </div>
    
                            <div class="post__content">
    
                                <div class="post__content-info">
    
                                        <h2 class="post__title entry-title text-center">
                                            <a href="{{ route('post.single',['slug' => $first_post->slug]) }}">{{ $first_post->title }}</a>
                                        </h2>
    
                                        <div class="post-additional-info">
    
                                            <span class="post__date">
    
                                                <i class="seoicon-clock"></i>
    
                                                <time class="published" datetime="2016-04-17 12:00:00">
                                                    {{ $first_post->created_at->diffForHumans() }}
                                                </time>
    
                                            </span>
    
                                            <span class="category">
                                                <i class="seoicon-tags"></i>
                                                <a href="{{ route('categoriespage', ['id' => $first_post->category->id]) }}">{{ $first_post->category->name }}</a>
                                            </span>
    
                                            <span class="post__comments">
                                                <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
                                                6
                                            </span>
    
                                        </div>
                                </div>
                            </div>
    
                    </article>
                </div>
                <div class="col-lg-2"></div>
            </div>
    
            {{-- Second and third post setting --}}
            <div class="row">
                <div class="col-lg-6">
                    <article class="hentry post post-standard has-post-thumbnail sticky">
    
                            <div class="post-thumb">
                                <img src="{{ $second_post->featured }}" alt="{{ $second_post->title }}">
                                <div class="overlay"></div>
                                <a href="{{ $second_post->featured }}" class="link-image js-zoom-image">
                                    <i class="seoicon-zoom"></i>
                                </a>
                                <a href="#" class="link-post">
                                    <i class="seoicon-link-bold"></i>
                                </a>
                            </div>
    
                            <div class="post__content">
    
                                <div class="post__content-info">
    
                                        <h2 class="post__title entry-title text-center">
                                            <a href="{{ route('post.single',['slug' => $second_post->slug]) }}">{{ $second_post->title }}</a>
                                        </h2>
    
                                        <div class="post-additional-info">
    
                                            <span class="post__date">
    
                                                <i class="seoicon-clock"></i>
    
                                                <time class="published" datetime="2016-04-17 12:00:00">
                                                    {{ $second_post->created_at->toFormattedDateString() }}
                                                </time>
    
                                            </span>
    
                                            <span class="category">
                                                <i class="seoicon-tags"></i>
                                                <a href="{{ route('categoriespage', ['id' => $second_post->category->id]) }}">{{ $second_post->category->name }}</a>
                                            </span>
    
                                            <span class="post__comments">
                                                <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
                                                6
                                            </span>
    
                                        </div>
                                </div>
                            </div>
    
                    </article>
                </div>
                <div class="col-lg-6">
                    <article class="hentry post post-standard has-post-thumbnail sticky">
    
                            <div class="post-thumb">
                                <img src="{{ $third_post->featured }}" alt="{{ $third_post->name }}">
                                <div class="overlay"></div>
                                <a href="{{ $third_post->featured }}" class="link-image js-zoom-image">
                                    <i class="seoicon-zoom"></i>
                                </a>
                                <a href="#" class="link-post">
                                    <i class="seoicon-link-bold"></i>
                                </a>
                            </div>
    
                            <div class="post__content">
    
                                <div class="post__content-info">
    
                                        <h2 class="post__title entry-title text-center">
                                            <a href="{{ route('post.single',['slug' => $third_post->slug]) }}">{{ $third_post->title }}</a>
                                        </h2>
    
                                        <div class="post-additional-info">
    
                                            <span class="post__date">
    
                                                <i class="seoicon-clock"></i>
    
                                                <time class="published" datetime="2016-04-17 12:00:00">
                                                   {{ $third_post->created_at->diffForHumans()}}
                                                </time>
    
                                            </span>
    
                                            <span class="category">
                                                <i class="seoicon-tags"></i>
                                                <a href="{{ route('categoriespage', ['id' => $third_post->category->id]) }}">{{ $third_post->category->name }}</a>
                                            </span>
    
                                            <span class="post__comments">
                                                <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i></a>
                                                6
                                            </span>
    
                                        </div>
                                </div>
                            </div>
    
                    </article>
                </div>
            </div>
        </div>
    
    {{-- A mettre ailleurs --}}
        <div class="container-fluid">
            <div class="row medium-padding120 bg-border-color">
                <div class="container">
                    <div class="col-lg-12">
                    <div class="offers">
                        <div class="row">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title"><a href="{{ route('categoriespage', ['id' => $jesus_love->id]) }}">{{ $jesus_love->name }}</a></h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            {{-- Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias ipsam vel ea, error consequatur totam dolorem asperiores explicabo ratione maiores. Temporibus beatae tempora quas libero a optio laudantium consectetur maxime! --}}
                            <div class="case-item-wrap">
                                @foreach ($jesus_love->posts()->orderBy('created_at', 'desc')->take(3)->get() as $post)
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="case-item">
                                            <div class="case-item__thumb">
                                                <img src="{{ $post->featured }}" alt="{{ $post->name }}">
                                            </div>
                                            <h6 class="case-item__title text-center"><a href="{{ route('post.single',['slug' => $post->slug]) }}">{{ $post->title }}</a></h6>
                                        </div>
                                    </div>
                                @endforeach
    
                            </div>
                        </div>
                    </div>
                    <div class="padded-50"></div>
                    <div class="offers">
                        <div class="row">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title"><a href="{{ route('categoriespage', ['id' => $ThreeJs->id]) }}">{{ $ThreeJs->name }}</a></h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="case-item-wrap">
                                @foreach ($ThreeJs->posts()->orderBy('created_at', 'desc')->take(3)->get() as $post)
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="case-item">
                                            <div class="case-item__thumb">
                                                <img src="{{ $post->featured }}" alt="{{ $post->name }}">
                                            </div>
                                            <h6 class="case-item__title text-center"><a href="{{ route('post.single',['slug' => $post->slug]) }}">{{ $post->title }}</a></h6>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="padded-50"></div>
                    {{-- Third category list post below that one --}}
                    {{-- Don't clean me!!! --}}
    
                    {{-- <div class="offers">
                        <div class="row">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                <div class="heading">
                                    <h4 class="h1 heading-title">Laravel 5.3</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="case-item-wrap">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="case-item">
                                        <div class="case-item__thumb">
                                            <img src="{{ asset('app/img/5.jpg') }}" alt="our case">
                                        </div>
                                        <h6 class="case-item__title"><a href="#">Investigationes demonstraverunt legere</a></h6>
                                    </div>
                                </div>
    
                                <div class="col-lg-4  col-md-4 col-sm-6 col-xs-12">
                                    <div class="case-item">
                                        <div class="case-item__thumb">
                                            <img src="{{ asset('app/img/2.png') }}" alt="our case">
                                        </div>
                                        <h6 class="case-item__title">Claritas est etiam processus dynamicus</h6>
                                    </div>
                                </div>
    
                                <div class="col-lg-4  col-md-4 col-sm-6 col-xs-12">
                                    <div class="case-item">
                                        <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                                            <img src="{{ asset('app/img/6.jpg') }}" alt="our case">
                                        </div>
                                        <h6 class="case-item__title">quod mazim placerat facer possim assum</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
    
                    {{-- Don't clean me!!! --}}
                    <div class="padded-50"></div>
                </div>
                </div>
            </div>
        </div>
@endsection