@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')
    
    <div class="card">
         <div class="card-header">
            Edit blog settings
        </div>
        <div class="card-body">
            <form action="{{ route('settings.update') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="Name">Site name</label>
                    <input type="text" name="site_name" value="{{ $setting->site_name }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="Number">Contact number</label>
                    <input type="text" name="contact_number" value="{{ $setting->contact_number }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="Email">Contact email</label>
                    <input type="email" name="contact_email" value="{{ $setting->contact_email }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="Address">Address</label>
                    <input type="text" name="address" value="{{ $setting->address }}" class="form-control">
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">
                            Update site setting
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection